//THESE FUNCTIONS ONLY UPDATE THE BUFFER, LEDS ARE ACTUALLY WRITTEN TO ONLY EXACTLY AFTER i2c COMMUNICATION TO AVOID BORK

void LedSelector(uint8_t mode, uint8_t data){
  switch(mode){
    case LED_OFF:
      OffLED(data);
      break;
    case LED_SPEED:
      SpeedStripeLED(data);
      break;
    case LED_CURRENT_SERVO:
      BarLED(data,255,0,0);
      break;
    case LED_CURRENT_MOTOR:
      BarLED(data,0,255,0);
      break;
    case LED_BATTERY:
      BarLED(data,255,102,0);
      break;
    default:
      //OffLED(data);
      SpeedStripeLED(data);
      //CurrentLED(data);
      break;
  }
}

void OffLED(uint8_t data){  //This just turns off all the leds
  static unsigned long stmp = 0;
  if(millis()-stmp > 30){
    stmp = millis();
    FastLED.clear();
  }
}

void SpeedStripeLED(uint8_t data){ //This makes a pulsing wave that (roughly) matches robot speed based on speed data
  static unsigned long stmp = 0;
  static float sinner = 0;
  const int baseline = 10; //Lowest bightness
  if(millis()-stmp > 30){
    for(byte i = 0; i < NUM_LEDS; i++) {
      //rightLeds[i] = CRGB(2,2,(sin(sinner+(i*((2*3.14)/16)))*(127-baseline))+(128+baseline));
      Leds[i] = CRGB(0,0,qadd8(quadwave8(sinner+8*i),5));
    }
    sinner += 5;
    //FastLED.show();
    stmp = millis();
  }
}

void BarLED(uint8_t data, uint8_t r, uint8_t g, uint8_t b){ //This uses the strip as a bar display to show current (or any other value really)
  static unsigned long stmp = 0;
  if(millis()-stmp>30){
    fill_solid(Leds, NUM_LEDS, CRGB::Black); //Blank all leds

    uint32_t pointsPerLed = 256/NUM_LEDS; //This many data points it takes to fill one led
    uint32_t fullLeds = data/pointsPerLed; //This many full leds there are
    uint32_t halfFullBright = data % pointsPerLed; //The remainder of the previous calculation, 0-PointsPerLed
    fill_solid(Leds, fullLeds, CRGB(r,g,b));
    if(fullLeds<NUM_LEDS) Leds[fullLeds] = CRGB(scale8(r,halfFullBright*(uint8_t)pointsPerLed),scale8(g,halfFullBright*(uint8_t)pointsPerLed),scale8(b,halfFullBright*(uint8_t)pointsPerLed));
    stmp = millis();
  }
}

//void CurrentLED(uint8_t data){ //This uses the strip as a bar display to show current (or any other value really)
//  static unsigned long stmp = 0;
//  if(millis()-stmp>30){
//    fill_solid(Leds, NUM_LEDS, CRGB::Black); //Blank all leds
//    //int barLength = (int)data / ((data+1) % NUM_LEDS); //Scale data to strip length
//
//    int pointsPerLed = 256/NUM_LEDS; //This many data points it takes to fill one led
//    int fullLeds = data/pointsPerLed; //This many full leds there are
//    int halfFullBright = data % pointsPerLed; //The remainder of the previous calculation, 0-PointsPerLed
//    
//    //int barLength = map(data,0,255,0,NUM_LEDS);
//    fill_solid(Leds, fullLeds, CRGB(255,0,0));
//    if(fullLeds<NUM_LEDS) Leds[fullLeds] = CRGB(scale8(255,halfFullBright*(uint8_t)pointsPerLed),0,0);
//
//    
////    int barLength = map(data,0,255,0,NUM_LEDS);
////    fill_solid(Leds, barLength, CRGB::Red);
//    stmp = millis();
//  }
//}
//void CurrentLED2(uint8_t data){ //This uses the strip as a bar display to show current (or any other value really)
//  static unsigned long stmp = 0;
//  if(millis()-stmp>30){
//    fill_solid(Leds, NUM_LEDS, CRGB::Black); //Blank all leds
//    //int barLength = (int)data / ((data+1) % NUM_LEDS); //Scale data to strip length
//
//    int pointsPerLed = 256/NUM_LEDS; //This many data points it takes to fill one led
//    int fullLeds = data/pointsPerLed; //This many full leds there are
//    int halfFullBright = data % pointsPerLed; //The remainder of the previous calculation, 0-PointsPerLed
//    
//    //int barLength = map(data,0,255,0,NUM_LEDS);
//    fill_solid(Leds, fullLeds, CRGB(0,255,0));
//    if(fullLeds<NUM_LEDS) Leds[fullLeds] = CRGB(0,scale8(255,halfFullBright*(uint8_t)pointsPerLed),0);
//    
//    //int barLength = map(data,0,255,0,NUM_LEDS);
//    //fill_solid(Leds, barLength, CRGB::Green);
//    stmp = millis();
//  }
//}
//void BatteryLed(uint8_t data){
//    static unsigned long stmp = 0;
//  if(millis()-stmp>30){
//    fill_solid(Leds, NUM_LEDS, CRGB::Black); //Blank all leds
//    //int barLength = (int)data / ((data+1) % NUM_LEDS); //Scale data to strip length
//    int pointsPerLed = 256/NUM_LEDS; //This many data points it takes to fill one led
//    int fullLeds = data/pointsPerLed; //This many full leds there are
//    int halfFullBright = data % pointsPerLed; //The remainder of the previous calculation, 0-PointsPerLed
//    
//    //int barLength = map(data,0,255,0,NUM_LEDS);
//    fill_solid(Leds, fullLeds, CRGB(255,102,0));
//    if(fullLeds<NUM_LEDS) Leds[fullLeds] = CRGB(scale8(255,halfFullBright*(uint8_t)pointsPerLed),scale8(102,halfFullBright*(uint8_t)pointsPerLed),0);
//    stmp = millis();
//  }
//}
