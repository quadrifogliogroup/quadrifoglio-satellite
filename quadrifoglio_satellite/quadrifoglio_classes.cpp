#include <Arduino.h>
#include "quadrifoglio_classes.h"

StoreFilter::StoreFilter(uint8_t pin, unsigned int startValue, float alpha){
  _pin = pin;
  value = startValue;
  _alpha = alpha;
  value = value;
}
float StoreFilter::filteredValue(){
  return _filtered;
}
void StoreFilter::runEMA(){
  _filtered = (_alpha*value)+((1-_alpha)*_filtered);
}
uint8_t StoreFilter::pin(){
  return _pin;
}

QuadriServo::QuadriServo(unsigned int lowPulse, unsigned int midPulse, unsigned int highPulse, float lowAngle, float highAngle){
  _lowPulse = lowPulse;
  _midPulse = midPulse;
  _highPulse = highPulse;
  _lowAngle = lowAngle;
  _highAngle = highAngle;
}

uint16_t QuadriServo::Degrees(float degIn){
  float safeInput = constrain(degIn, _lowAngle, _highAngle); //Accept only values that are in range of the servo
  _pulse = _midPulse;
  if(safeInput>0){
    _pulse = map((long)(safeInput*100), 0, (long)(_highAngle*100), _midPulse, _highPulse);
  }
  else{
    _pulse = map((long)(safeInput*100), 0, (long)(_lowAngle*100), _midPulse, _lowPulse);
  }
  
  return _pulse;
}

uint16_t QuadriServo::Range1024(int input){
  int16_t safeInput = constrain(input, -1024, 1024);
  _pulse = _midPulse;
  if(safeInput >0){
    _pulse = map(safeInput,1,1024,_midPulse,_highPulse);
  }
  else{
    _pulse = map(safeInput,0,-1024,_midPulse,_lowPulse);
  }
  return _pulse;
}

uint16_t QuadriServo::LowPulse(){
  return _lowPulse;
}
uint16_t QuadriServo::MidPulse(){
  return _midPulse;
}
uint16_t QuadriServo::HighPulse(){
  return _highPulse;
}
uint16_t QuadriServo::Pulse(){
  return _pulse;
}

float QuadriServo::GetDegrees(){
  float out = 0.0;
  
  if(_pulse > _midPulse){  //If past zero
    out = ((float)map(_pulse, _midPulse, _highPulse, 0, (long)(_highAngle*100)))/100;
  }
  else{
    out = ((float)map(_pulse, _midPulse, _lowPulse, 0, (long)(_lowAngle*100)))/100;
  }
  return out;
}
