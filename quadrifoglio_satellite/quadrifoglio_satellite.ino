#define MAIN_UC 0x03 //I2C addresses
#define FRONT_ULTRA 0x04
#define REAR_ULTRA 0x05
#define RASPI 0x07

#define ISFRONT //Pick one. Both use uno bootloader
//#define ISREAR

#ifdef ISFRONT
uint8_t ident = FRONT_ULTRA;   
#endif

#ifdef ISREAR
uint8_t ident = REAR_ULTRA;   
#endif

#include <Wire.h> 
#include "FastLED.h"
#include <NewPing.h>
//#include <Transcutale_I2C_Slave.h>
#include "quadrifoglio_classes.h"

bool scanOn = true;

byte servopin = 10;
unsigned int servo_center = 3000;
unsigned int servo_low = 1200;
unsigned int servo_high = 4800;
unsigned int possu = servo_center;

#ifdef ISFRONT
QuadriServo Servo1(1840,2750,3986,-45,45);  //Different servo calibration for both
#endif

#ifdef ISREAR
QuadriServo Servo1(1840,2720,3886,-45,45);
#endif

byte left_trig = 9;
byte left_echo = 8;
byte right_trig = 4;
byte right_echo = 2;
byte center_trig = 7;
byte center_echo = 3;
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

#define NUM_LEDS 16
#define ledPin 17
#define boardLedPin 13
CRGB Leds[NUM_LEDS];
boolean got = false;
#define LED_OFF 0  //Possible states for ledstrips driven by front and rear ultras
#define LED_SPEED 1
#define LED_CURRENT_SERVO 2
#define LED_CURRENT_MOTOR 3
#define LED_BATTERY 4

struct LedMessage{
  uint8_t mode; //This tells what the leds should do. 0-Off 1-SpeedMode 2-CurrentMode
  uint8_t modeData; //This is some mode-specific information (speed in speed mode, current in current mode and so on
  uint8_t scanMode;  //Autonomous ultrasound scan or high level control
  uint16_t servoPos; //Servo position for high level control
};

struct SonarMessage{
  uint8_t which; //This tells from which ultrasound sensor the measurement was taken
  uint16_t meas; //This is the actual distance measurement in cm
  uint32_t chronitron;  //WHEN (according to the uc's clock) this measurement was taken, in milliseconds after boot
  int16_t servo;        //Position of servo, in degrees from center
};

LedMessage ledMsg = {0,0,0,30000};
SonarMessage sonarMsg = {0,0,0,servo_center};


unsigned int loopRate = 0;
NewPing sonarLeft(left_trig, left_echo, MAX_DISTANCE);
NewPing sonarRight(right_trig, right_echo, MAX_DISTANCE);
NewPing sonarCenter(center_trig, center_echo, MAX_DISTANCE);
unsigned int sonarLeft_cm = 0;
unsigned int sonarRight_cm = 0;
unsigned int sonarCenter_cm = 0;

void setup() {
  //Timer 1 for center ultra direction servo, which is on arduino pin 10, OC1B
  pinMode(servopin ,OUTPUT); //Servo pin to output
  TCCR1A = _BV(WGM11) | _BV(COM1B1);  //SET WGM's to fast PWM mode with ICR top, COM3 to non inverting mode
  TCCR1B =  _BV(WGM12) | _BV(WGM13) | _BV(CS11);  //Prescaler to 8
  ICR1 = 39999;//this is TOP, set to 39999 making a 50 hz pwm signal for standard servos
  OCR1B = 3000; //1 ms servo signal

  
  pinMode(boardLedPin, OUTPUT);
  //Serial.begin(115200); // start serial for output
  // initialize i2c as slave
  Wire.begin(ident);

  // define callbacks for i2c communication
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  FastLED.addLeds<NEOPIXEL, ledPin >(Leds, NUM_LEDS);
  LEDS.setBrightness(255);
  delay(50);
//  fill_solid(Leds, NUM_LEDS, CRGB::White);
//  FastLED.show();
//  delay(3000);
//  fill_solid(Leds, NUM_LEDS, CRGB::Black);
//  FastLED.show();
}

void loop() {
  
  LedSelector(ledMsg.mode, ledMsg.modeData);
  AfterComms();
}

void ServoSet(unsigned int input){
  OCR1B = input;
}

void LoopRateChecker(){
  static unsigned long stamppi = 0;
  static unsigned int loops = 0;
  loops++;
  unsigned long current = millis();
  if(current-stamppi > 999){
    stamppi = current;
    loopRate = loops;
    loops = 0;
  }
}
// callback for received data
void receiveData(int byteCount) {

  GetAnything(ledMsg);
}

// callback for sending data
void sendData(byte which) {
  //SendAnythingSlave(sonarMsg);  //Send the current sonar message. This has been updated in the SonarPinger function
  SendAnythingSlave(sonarMsg);
  got=true;                     //which runs right after send/receive communications, and should be completed before this
}

template <class T> int GetAnything(const T& value){
    byte* p = (byte*)(void*)&value;
    unsigned int i;
    for (i = 0; i < sizeof(value); i++){ //Read until struct is full
      if(Wire.available()) *p++ = Wire.read(); //Read if there is stuff incoming, else abort
      else return -1;
    }
    return i;
}

template <class T> int SendAnythingSlave( const T& value){
    const byte* p = (const byte*)(const void*)&value;
    unsigned int i;
    byte checksum = 0;
    for (i = 0; i < sizeof(value); i++){ //Stop one byte short of strservocurrentuct end, last byte is xor
      const byte b = *p;
      checksum ^= b;
      Wire.write(b), p++;
    }
    //Wire.write(checksum);
    //return i+1;
}

void AfterComms(){ //Assumes i2c message is received and another is requested immediately, this then runs timing critical stuff after that
  if(got){
    got = false;
    SonarPinger();
    FastLED.show();
    //ServoSet(map(ledMsg.servoPos,28976,31024,500,5500));
    //ServoSet(Servo1.Degrees(((float)(map(ledMsg.servoPos,28976,31024,-4500,4500))))/100.0);
    float bigDegs = (float)map(ledMsg.servoPos,28976,31024,-4500,4500);
    ServoSet(Servo1.Degrees(bigDegs/100));
    digitalWrite(boardLedPin,!digitalRead(boardLedPin));  //Toggle led when msg received
    //Serial.print(OCR1B); Serial.print('\t'); Serial.println(ledMsg.servoPos);
  }
}

void SonarPinger(){
  static uint8_t which = 1; //1 is left, 2 is right and 3 is center
  if(which == 1){
    sonarLeft_cm = sonarLeft.ping_cm();
    sonarMsg.meas = sonarLeft_cm;
    sonarMsg.which = 1;
    which = 2;
  }
  else if(which == 2){
    sonarRight_cm = sonarRight.ping_cm();
    sonarMsg.meas = sonarRight_cm;
    sonarMsg.which = 2;
    which = 3;
  }
  else if(which == 3){
    sonarCenter_cm = sonarCenter.ping_cm();
    sonarMsg.meas = sonarCenter_cm;
    sonarMsg.which = 3;
    which = 1;
  }
  else{
    
  }
  sonarMsg.chronitron = millis(); //When the measurement was taken
  sonarMsg.servo = (int16_t)Servo1.GetDegrees();
}
